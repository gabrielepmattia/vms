# My VMs

_A set of my preferred and ready to go VMs_

| Name                    | Type   | Zip Size  | Description       |
| :---------------------: | :----: | :-------: | :---------------: |
| arch-aosv_base.vmwarevm | VMWare | 1.3GB     | ArchLinux with i3 |

## Instruction and notes
- **arch-aosv_base.vmwarevm** - Start the UI with `startx` command. Root user has no password